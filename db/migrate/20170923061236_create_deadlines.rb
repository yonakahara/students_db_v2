class CreateDeadlines < ActiveRecord::Migration
  def change
    create_table :deadlines do |t|
      t.string :deadline

      t.timestamps null: false
    end
  end
end
