class StudentsController < ApplicationController
  def index
    @students = Student.all
    @students = Student.all.order(created_at: :desc)
  end
  
  def create
    @students = Student.create(name: params[:name])
    redirect_to("/")
  end
  
  def date_select
  end
  
  def date_new
    @deadline = Deadline.create(deadline: params[:deadline])
    redirect_to("/date_select")
  end
  
end
